import { IconsList } from './icons';

interface IIcon {
  fill: string;
  stroke: string;
  html: string;
  strokeWidth: number;
  create(): string;
}

export class DropIcon implements IIcon {
  html: string;

  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) {
  }

  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getDrop()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class DropIconFooter implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) {

  }
  create(): string {
    let ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getDrop();
    this.html = ico.header;
    this.html += ico.svg;
    ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getFooter();
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class DiamondIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getDiamond()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class BusIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getBus()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class HeartIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getHeart()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class BowlIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getBowl()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class BarbellIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getBarbell()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class StarIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getStar()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class FoodIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getFood()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class DollarIcon implements IIcon {
  html: string;
  constructor(
    public fill: string = '#000',
    public stroke: string = '#000',
    public strokeWidth: number = 10) { }
  create(): string {
    const ico = new IconsList(this.fill, this.stroke, this.strokeWidth).getDollar()
    this.html = ico.header;
    this.html += ico.svg;
    this.html += '</svg>';
    return this.html;
  }
}

export class CreateIcon {
  constructor(private icon: IIcon) { }
  create() {
    return this.icon.create();
  }
}

export class ConcatIcon {
  constructor(private iconExt: IIcon, private iconInner: IIcon) {

  }
  create(): string {
    const icon1 = this.iconExt.create(),
      icon2 = this.iconInner.create();
    return `<div class="icon-container" style="position:relative;">${icon1}<div class="icon-inner" style="position:absolute;left: 3px;top: 2px;width: 80%;">${icon2}</div></div>`
  }
}