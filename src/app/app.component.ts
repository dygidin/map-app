import { Component, ViewChild } from '@angular/core';
import { MapComponent } from './map/map.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  count: number = 100;
  @ViewChild('mapCmp', {
    static: true
  }) map: MapComponent;

  onSubmit() {
    this.map.addRandomMarkets(this.count);
  }

  clear() {
    this.map.clearMarkers();
  }
}
