import { Component, OnInit, Input } from '@angular/core';
import * as L from 'leaflet';
import { DropIcon, DropIconFooter, ConcatIcon, DiamondIcon, BusIcon, HeartIcon, DollarIcon, BarbellIcon, BowlIcon, StarIcon, FoodIcon } from '../types/icon';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  private map;
  private arrMarkers = [];
  constructor() { }

  private initMap(): void {
    this.map = L.map('map', {
      center: [59.9343, 30.3351],
      zoom: 15
    });
    // const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //   maxZoom: 19,
    //   attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    // });
    const tiles = L.tileLayer('https://api.maptiler.com/maps/basic/{z}/{x}/{y}.png?key=BuNi4FPIgsaSVnVlaLoQ', { maxZoom: 19, attribution: 'https://cloud.maptiler.com/maps/basic/openlayers', crossOrigin: true })
    tiles.addTo(this.map);
  }

  public addRandomMarkets(number: number) {
    const bounds = this.map.getBounds(),
      southWest = bounds.getSouthWest(),
      northEast = bounds.getNorthEast(),
      lngSpan = northEast.lng - southWest.lng,
      latSpan = northEast.lat - southWest.lat,
      pointsrand = [];

    for (let i = 0; i < number; ++i) {
      let point = [southWest.lat + latSpan * Math.random(), southWest.lng + lngSpan * Math.random()];
      pointsrand.push(point);
    }

    for (let i = 0; i < number; ++i) {
      let str_text = i + " : " + pointsrand[i];
      let marker = this.placeMarker(pointsrand[i], str_text);
      marker.addTo(this.map);
      this.arrMarkers.push(marker);
    }
  }

  private getRandomColor(): string {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  betweenRandom(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  private getRandomIcon(): ConcatIcon {

    const iconList = [
      new DollarIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
      new HeartIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
      new BarbellIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
      new BowlIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
      new StarIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
      new DiamondIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
      new FoodIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
      new BusIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(1, 5)),
    ];

    const icon = this.betweenRandom(1, 2) == 1 ? new DropIconFooter(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(20, 40)) : new DropIcon(this.getRandomColor(), this.getRandomColor(), this.betweenRandom(20, 40));
    return new ConcatIcon(icon, iconList[Math.floor(Math.random() * iconList.length)]);
  }

  private placeMarker(location, text) {
    const icon = this.getRandomIcon(),
      myIcon = L.divIcon({
        className: 'my-div-icon',
        iconSize: [50, 50], html: icon.create()
      }),
      marker = L.marker(location, { text: text, icon: myIcon });
    return marker;
  }

  public clearMarkers() {
    if (this.arrMarkers) {
      for (let i in this.arrMarkers) {
        this.arrMarkers[i].removeFrom(this.map);
      }
    }
    this.arrMarkers = new Array(0);
  }

  ngOnInit() { }

  ngAfterViewInit(): void {
    this.initMap();
  }

}
